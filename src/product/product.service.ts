import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

const product: Product[] = [
  { id: 1, name: 'Administrator', price: 123 },
  { id: 2, name: 'User 1', price: 134 },
  { id: 3, name: 'User 2', price: 152 },
];
let lastProductId = 4;
@Injectable()
export class ProductService {
  create(createProductDto: CreateProductDto) {
    console.log({ ...createProductDto });
    const newProduct: Product = {
      id: lastProductId++,
      ...createProductDto,
    };
    return 'This action adds a new product';
  }

  findAll() {
    return product;
  }

  findOne(id: number) {
    return `This action returns a #${id} product`;
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    return `This action updates a #${id} product`;
  }

  remove(id: number) {
    return `This action removes a #${id} product`;
  }
}
